const
    axios = require('axios'),
    { Window } = require('happy-dom'),
    client = axios.create({ baseURL: 'https://www.gsmarena.com' });

client.interceptors.response.use(response => {
    const window = new Window();
    window.document.documentElement.innerHTML = response.data;
    response.document = window.document;
    return response;
});

module.exports = {
    search: async ({
        query
    }) => {
        const {
            document
        } = await client(
            'res.php3',
            {
                params: {
                    'sSearch': query
                }
            }
        );
        return document.querySelectorAll('.makers a').map(item => {
            const { childNodes } = item.querySelector('span');
            return {
                id: item.getAttribute('href').slice(0, -4),
                picture: item.querySelector('img').getAttribute('src'),
                brand: childNodes[0].textContent,
                model: childNodes[2].textContent
            };
        });
    },
    get: async ({
        id
    }) => {
        const {
            document
        } = await client(
            `${id}.php`
        );
        return {
            specs: document
                .querySelectorAll('[data-spec]')
                .reduce((data, item) => ({
                    ...data,
                    [item.getAttribute('data-spec')]: item
                        .textContent
                        .trim()
                        .replace(/\s&nbsp;/g, '')
                        .split(/\r?\n/)
                        .map(item => item.split(', '))
                }), {}),
            versions: document.querySelectorAll('[data-version]:not([data-version="*"])')
                .map(item => item.getAttribute('data-version'))
        }
    }
};